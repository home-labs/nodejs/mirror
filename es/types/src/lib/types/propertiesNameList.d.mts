import { UnionSet2RandomList } from '@cyberjs.on/types';
export type PropertiesNameList<T extends (Function | Object), K = keyof T> = UnionSet2RandomList<K>;

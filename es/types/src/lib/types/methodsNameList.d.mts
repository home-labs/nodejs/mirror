import { TupleShift, TuplePush } from '@cyberjs.on/types';
import { PropertiesNameList } from './propertiesNameList.mjs';
export type MethodsNameList<T extends (Function | Object), P extends any[] = PropertiesNameList<T>, R extends any[] = []> = P['length'] extends 0 ? R : T[P[0]] extends Function ? MethodsNameList<T, TupleShift<P>, TuplePush<R, P[0]>> : MethodsNameList<T, TupleShift<P>, R>;

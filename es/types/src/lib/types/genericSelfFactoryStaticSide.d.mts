export interface GenericSelfFactoryStaticSide<C> {
    new (...args: any): C;
    getOwnInstance(this: {
        new (...args: any): C;
    }, ...args: any): C;
}

import { TupleShift, TuplePush } from '@cyberjs.on/types';
import { PropertiesNameList } from './propertiesNameList.mjs';
export type FieldsNameList<T extends (Function | Object), P extends any[] = PropertiesNameList<T>, R extends any[] = []> = P['length'] extends 0 ? R : T[P[0]] extends Function ? FieldsNameList<T, TupleShift<P>, R> : FieldsNameList<T, TupleShift<P>, TuplePush<R, P[0]>>;

import { GenericSelfFactoryStaticSide } from "./index.mjs";
import { InstanceSideType } from "./instanceSideType.mjs";
export interface SelfFactoryStaticSide extends GenericSelfFactoryStaticSide<InstanceSideType> {
    new <T extends SelfFactoryStaticSide, U extends InstanceType<T>>(...args: any): U;
    getOwnInstance<T extends SelfFactoryStaticSide, U extends InstanceType<T>>(this: {
        new (...args: any): U;
    }, ...args: any): U;
}

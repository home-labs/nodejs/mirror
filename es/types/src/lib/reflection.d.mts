import { GenericSelfFactoryStaticSide } from './types/index.mjs';
import { FieldsNameList } from './types/fieldsNameList.mjs';
import { MethodsNameList } from './types/methodsNameList.mjs';
import { InstanceSideType } from './types/instanceSideType.mjs';
export declare class Reflection<T extends GenericSelfFactoryStaticSide<InstanceSideType>> {
    #private;
    constructor(constructorReference: T);
    get methodsName(): MethodsNameList<InstanceType<T>>;
    get fieldsName(): FieldsNameList<InstanceType<T>>;
    get staticMethodsName(): MethodsNameList<T>;
    get staticFieldsName(): FieldsNameList<T>;
    static getConstructorsOfPrototypeChain(constructorReference: Function, catchNative?: boolean): Function[];
}

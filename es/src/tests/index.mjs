import { SelfFactory, Reflection } from '../index.mjs';
const symbol1 = Symbol();
const symbol2 = Symbol();
const symbolAsMethod1 = Symbol();
const symbolAsMethod2 = Symbol();
const sSymbol1 = Symbol();
const sSymbol2 = Symbol();
const sSymbolAsMethod1 = Symbol();
const sSymbolAsMethod2 = Symbol();
const C1 = class C1 extends SelfFactory {
    p1 = 1;
    p2 = 2;
    #pp1;
    static sp1 = 1;
    static sp2 = 2;
    static #psp1 = `this is a static property defined in ${this.name}`;
    constructor(...args) {
        super();
        this.#pp1 = `this is a property defined in ${C1.name}`;
    }
    m1() {
    }
    m2() {
    }
    #pm1() {
    }
    [symbol1] = 1;
    [symbol2] = 2;
    [symbolAsMethod1]() {
        return 'The symbol as method 1';
    }
    [symbolAsMethod2]() {
        return 'The symbol as method 2';
    }
    static sm1() {
    }
    static {
        this.sm1 = () => {
        };
    }
    static sm2() {
    }
    static #psm1() {
        return this.#psp1;
    }
    static [sSymbol1] = 1;
    static [sSymbol2] = 2;
    static [sSymbolAsMethod1]() {
        return 'The static symbol as method 1';
    }
    static [sSymbolAsMethod2]() {
        return 'The static symbol as method 2';
    }
};
C1.getOwnInstance();
const reflection = new Reflection(C1);
console.log('atributos de instância:', reflection.fieldsName);
console.log('métodos de instância:', reflection.methodsName);
console.log('atributos de classe:', reflection.staticFieldsName);
console.log('métodos de classe:', reflection.staticMethodsName);
//# sourceMappingURL=index.mjs.map
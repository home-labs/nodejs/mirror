var _a;
export class Reflection {
    #_fieldsName;
    #_methodsName;
    #_staticFieldsName;
    #_staticMethodsName;
    constructor(constructorReference) {
        const staticPropertiesList = this
            .#getStaticPropertiesList(constructorReference);
        this.#_fieldsName = this.#resolveFieldsName(constructorReference);
        this.#_staticFieldsName = this
            .#resolveStaticFieldsName(constructorReference, staticPropertiesList);
        this.#_staticMethodsName = this
            .#resolveStaticMethodsName(constructorReference, staticPropertiesList);
        this.#_methodsName = this.#resolveMethodsName(constructorReference);
    }
    get methodsName() {
        return this.#_methodsName;
    }
    get fieldsName() {
        return this.#_fieldsName;
    }
    get staticMethodsName() {
        return this.#_staticMethodsName;
    }
    get staticFieldsName() {
        return this.#_staticFieldsName;
    }
    #resolveFieldsName(constructorReference) {
        return Reflect.ownKeys(constructorReference.getOwnInstance());
    }
    #resolveStaticFieldsName(constructorReference, staticPropertiesList) {
        return staticPropertiesList.filter(propertyReference => typeof constructorReference[propertyReference] !== 'function');
    }
    #resolveMethodsName(constructorReference) {
        const _self = _a;
        let constructorsOfPrototypeChain;
        const methods = Reflect
            .ownKeys(constructorReference.prototype)
            .filter(propertyReference => propertyReference !== 'constructor');
        constructorsOfPrototypeChain = _self
            .getConstructorsOfPrototypeChain(constructorReference);
        constructorsOfPrototypeChain.forEach(constructorOfParent => {
            methods.push(...Reflect.ownKeys(constructorOfParent.prototype)
                .filter(propertyReference => propertyReference !== 'constructor'));
        });
        return methods;
    }
    #resolveStaticMethodsName(constructorReference, staticPropertiesList) {
        return staticPropertiesList.filter(propertyReference => typeof constructorReference[propertyReference] === 'function');
    }
    #getStaticPropertiesList(constructorReference) {
        const _self = _a;
        const propertyReferences = this
            .#filterPropertynames(Reflect.ownKeys(constructorReference));
        let constructorsOfPrototypeChain;
        constructorsOfPrototypeChain = _self
            .getConstructorsOfPrototypeChain(constructorReference);
        constructorsOfPrototypeChain.forEach(constructorOfParent => {
            propertyReferences.push(...this.#filterPropertynames(Reflect.ownKeys(constructorOfParent))
                .filter(propertyReference => propertyReference !== 'prototype'));
        });
        return propertyReferences;
    }
    #filterPropertynames(propertyReferences) {
        return propertyReferences
            .filter(propertyReference => propertyReference !== 'length'
            && propertyReference !== 'name');
    }
    static getConstructorsOfPrototypeChain(constructorReference, catchNative = false) {
        const parents = [];
        let parent = Object.getPrototypeOf(constructorReference);
        while (parent) {
            if (parent instanceof Function) {
                parents.push(parent);
                parent = Object.getPrototypeOf(parent);
            }
            else {
                if (catchNative) {
                    parents.push(Object);
                    parents.push(Function);
                }
                parent = null;
            }
        }
        return parents;
    }
}
_a = Reflection;
//# sourceMappingURL=reflection.mjs.map
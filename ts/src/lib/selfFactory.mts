import { SelfFactoryStaticSide } from "./types/selfFactoryStaticSide.mjs";


export const SelfFactory: SelfFactoryStaticSide = class SelfFactory {

    constructor(...args: any) {

    }

    static getOwnInstance<
        /**
         *
         * usa-se a chamada ao typeof quando queremos referenciar o tipo (abstração) de uma
         * classe concreta.
         */
        T extends typeof SelfFactory,
        U extends InstanceType<T>
    >(
        this: {
            new(...args: any): U
        },
        ...args: any
    ): U
    {
        return new this(...args);
    }

}

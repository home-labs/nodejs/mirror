import { GenericSelfFactoryStaticSide } from './types/index.mjs';
import { FieldsNameList } from './types/fieldsNameList.mjs';
import { MethodsNameList } from './types/methodsNameList.mjs';
import { InstanceSideType } from './types/instanceSideType.mjs';


export class Reflection<
    T extends GenericSelfFactoryStaticSide<InstanceSideType>
> {

    #_fieldsName!: FieldsNameList<InstanceType<T>>;

    #_methodsName!: MethodsNameList<InstanceType<T>>;

    #_staticFieldsName: (string | symbol)[];

    #_staticMethodsName!: MethodsNameList<T>;

    constructor(
        constructorReference: T
    ) {

        const staticPropertiesList = this
            .#getStaticPropertiesList(constructorReference);

        this.#_fieldsName = this.#resolveFieldsName(constructorReference);

        this.#_staticFieldsName = this
            .#resolveStaticFieldsName(constructorReference, staticPropertiesList);

        this.#_staticMethodsName = this
            .#resolveStaticMethodsName(constructorReference, staticPropertiesList);

        this.#_methodsName = this.#resolveMethodsName(constructorReference);
    }

    get methodsName(): MethodsNameList<InstanceType<T>> {
        return this.#_methodsName;
    }

    get fieldsName(): FieldsNameList<InstanceType<T>> {
        return this.#_fieldsName;
    }

    get staticMethodsName(): MethodsNameList<T> {
        return this.#_staticMethodsName;
    }

    get staticFieldsName(): FieldsNameList<T> {
        return this.#_staticFieldsName as any;
    }

    #resolveFieldsName(constructorReference: T): FieldsNameList<InstanceType<T>> {
        return Reflect.ownKeys(constructorReference.getOwnInstance()) as any;
    }

    #resolveStaticFieldsName(
        constructorReference: Function,
        staticPropertiesList: (string | symbol)[]
    ): (string | symbol)[] {

        return staticPropertiesList.filter(
            propertyReference =>
                typeof (constructorReference as any)[propertyReference] !== 'function'
        ) as any;
    }

    #resolveMethodsName(constructorReference: Function): MethodsNameList<InstanceType<T>> {

        const _self = Reflection;

        let constructorsOfPrototypeChain: Function[];

        const methods = Reflect
            .ownKeys(constructorReference.prototype)
            .filter(
                propertyReference => propertyReference !== 'constructor'
            ) as any;

        constructorsOfPrototypeChain = _self
            .getConstructorsOfPrototypeChain(constructorReference);

        constructorsOfPrototypeChain.forEach(
            constructorOfParent => {
                methods.push(...Reflect.ownKeys(constructorOfParent.prototype)
                    .filter(
                        propertyReference => propertyReference !== 'constructor'
                    )
                )
            }
        );

        return methods;
    }

    #resolveStaticMethodsName(
        constructorReference: Function,
        staticPropertiesList: (string | symbol)[]
    ): MethodsNameList<T> {

        return staticPropertiesList.filter(
            propertyReference =>
                typeof (constructorReference as any)[propertyReference] === 'function'
        ) as any;
    }

    #getStaticPropertiesList(constructorReference: Function): (string | symbol)[] {

        const _self = Reflection;

        /**
         *
         * the ownKeys method catch a non-enumerable propertyReferences, and static and symbols
         * members
         */
        const propertyReferences = this
            .#filterPropertynames(Reflect.ownKeys(constructorReference));

        let constructorsOfPrototypeChain: Function[];

        constructorsOfPrototypeChain = _self
            .getConstructorsOfPrototypeChain(constructorReference);

        constructorsOfPrototypeChain.forEach(
            constructorOfParent => {
                propertyReferences.push(...
                    this.#filterPropertynames(Reflect.ownKeys(constructorOfParent))
                    .filter(
                        propertyReference => propertyReference !== 'prototype'
                    )
                )
            }
        );

        return propertyReferences;
    }

    #filterPropertynames(propertyReferences: (string | symbol)[]) {
        return propertyReferences
            .filter(
                propertyReference =>
                    propertyReference !== 'length'
                    && propertyReference !== 'name'
            );
    }

    static getConstructorsOfPrototypeChain(
        constructorReference: Function,
        catchNative = false
    ): Function[] {

        const parents: Function[] = [];

        let parent = Object.getPrototypeOf(constructorReference);

        while (parent) {
            if (parent instanceof Function) {
                parents.push(parent);
                parent = Object.getPrototypeOf(parent);
            } else {
                if (catchNative) {
                    parents.push(Object);
                    parents.push(Function);
                }

                parent = null;
            }
        }

        return parents;
    }

}

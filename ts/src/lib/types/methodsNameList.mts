import {
    TupleShift,
    TuplePush
} from '@cyberjs.on/types';

import { PropertiesNameList } from './propertiesNameList.mjs';


export type MethodsNameList<
    T extends (Function | Object),
    P extends any[] = PropertiesNameList<T>,
    R extends any[] = []
> =
    P['length'] extends 0
        ? R
        : T[P[0]] extends Function
            ? MethodsNameList<T, TupleShift<P>, TuplePush<R, P[0]>>
            : MethodsNameList<T, TupleShift<P>, R>


/*
class C1 {

    p1 = 1;

    p2 = 2;

    #pp1 = 1;

    #pp2 = 2;

    static sp1 = 1;

    static sp2 = 2;

    static #psp1 = 1;

    static #psp2 = 2;

    m1() {

    }

    m2() {

    }

    static sm1() {

    }

    static sm2() {

    }

}


let methodsName: MethodsNameList<C1>

let staticMethodsName: MethodsNameList<typeof C1>
/**/

export interface GenericSelfFactoryStaticSide<C> {

    // here is the secret to reference the static side
    new(...args: any): C;

    getOwnInstance(
        this: {
            new(...args: any): C;
        }, ...args: any
    ): C;

};

import { GenericSelfFactoryStaticSide } from "./index.mjs";
import { InstanceSideType } from "./instanceSideType.mjs";


export interface SelfFactoryStaticSide
    extends GenericSelfFactoryStaticSide<InstanceSideType>
{

    new <
        T extends SelfFactoryStaticSide,
        U extends InstanceType<T>
    >(...args: any): U;

    getOwnInstance<
        /**
         *
         * esta interface, como informa o próprio nome, referencia o lado estático
         * de quem a implementa, logo, neste caso, o this se refere ao lado estático, sendo
         * o próprio SelfFactoryStaticSide, que estende GenericSelfFactoryStaticSide.
         */
        T extends SelfFactoryStaticSide,
        U extends InstanceType<T>
    >(
        this: {
            new(...args: any): U
        },
        ...args: any
    ): U;

}

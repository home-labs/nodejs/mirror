/**
 *
 * Decerto que para referenciar uma constante como um tipo dentro de uma estrutura
 * abstrata, como uma interface ou uma classe abstrata, deve-se usar a declaração "typeof".
 * Isso não é necessário quando se usa uma classe comum como tipo, porque, para toda classe
 * declarada, o TS declara uma interface implicitamente.
 * Isto pode ser comprovado ao se usar a declaração "implements" referenciando outra classe
 * comum, o que estará sendo referenciado é a interface desta classe.
 */
export interface InstanceSideType { }

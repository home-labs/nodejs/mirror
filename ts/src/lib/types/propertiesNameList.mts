import { UnionSet2RandomList } from '@cyberjs.on/types';


export type PropertiesNameList<T extends (Function | Object), K = keyof T> = UnionSet2RandomList<K>;


/*
const symb1 = Symbol();
const symb2 = Symbol();
class C1 {

    static sp1 = 1;

    static sp2 = 2;

    p1 = 1;

    p2 = 2;

    static #psp1 = 1;

    static #psp2 = 2;

    #pp1 = 1;

    #pp2 = 2;

    m2() {

    }

    m1() {

    }

    static sm2() {

    }

    static sm1() {

    }

    static [symb1]() {

    }

    [symb1]() {

    }

}


let staticMembers: PropertiesNameList<typeof C1>;
staticMembers[0] //=> prototype

let instanceMembers: PropertiesNameList<C1>;
instanceMembers[0] //=> p1
/**/

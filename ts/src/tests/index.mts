import {
    GenericSelfFactoryStaticSide,
    SelfFactory,
    Reflection
} from '../index.mjs';


const symbol1 = Symbol();

// console.log(typeof symbol1, '\n'); //=> symbol

const symbol2 = Symbol();

const symbolAsMethod1 = Symbol();

const symbolAsMethod2 = Symbol();

const sSymbol1 = Symbol();

const sSymbol2 = Symbol();

const sSymbolAsMethod1 = Symbol();

const sSymbolAsMethod2 = Symbol();

interface InstanceSide1 {

    p1: number;

    m1(): void;

    [symbol1]: number;

    [symbolAsMethod1](): string;

}

interface InstanceSide2 {

    p2: number;

    m2(): void;

    [symbol2]: number;

    [symbolAsMethod2](): string;

}

/**
 *
 * se acrescentar o InstanceSide1 no extends, as propriedades de instancia dele se tornam
 * propriedades de classe quando no extends houver SelfFactoryStaticSide
 */
// interface StaticSide1 extends SelfFactoryStaticSide, InstanceSide1 {

interface StaticSide1 extends GenericSelfFactoryStaticSide<InstanceSide1 & InstanceSide2> {

    sp1: number;

    [sSymbol1]: number;

    sm1(): void;

    [sSymbolAsMethod1](): string;

}

interface StaticSide2 extends GenericSelfFactoryStaticSide<InstanceSide1 & InstanceSide2> {

    sp2: number;

    [sSymbol2]: number;

    sm2(): void;

    [sSymbolAsMethod2](): string;

}

const C1: StaticSide1 & StaticSide2 = class C1 extends SelfFactory {

    p1 = 1;

    p2 = 2;

    #pp1: string;

    static sp1 = 1;

    static sp2 = 2;

    static #psp1 = `this is a static property defined in ${this.name}`;

    constructor(...args: any) {
        super();

        this.#pp1 = `this is a property defined in ${C1.name}`;
    }

    m1() {

    }

    m2() {

    }

    #pm1() {

    }

    [symbol1] = 1;

    [symbol2] = 2;

    [symbolAsMethod1](): string {
        return 'The symbol as method 1';
    }

    [symbolAsMethod2](): string {
        return 'The symbol as method 2';
    }

    static sm1() {

    }

    /**
     *
     * static initialization blocks em TS só serve para referenciar membros estáticos
     * previamente declarados, a fim de inicializá-los ao invés de fazê-lo diretamente na
     * declaração. Contudo, precisar ser declarado antes torna a solução redundante, já que,
     * neste caso, ela é redeclarada dentro do bloco.
     */
    static {

        this.sm1 = () => {

        }

    }

    static sm2() {

    }

    static #psm1() {
        return this.#psp1;
    }

    static [sSymbol1] = 1;

    static [sSymbol2] = 2;

    static [sSymbolAsMethod1](): string {
        return 'The static symbol as method 1';
    }

    static [sSymbolAsMethod2](): string {
        return 'The static symbol as method 2';
    }

}


C1.getOwnInstance() //=> InstanceSide1 & InstanceSide2

/**
 *
 * em relação ao símbolo, o tipo mapeia para o tipo do símbolo (typeof)
 */
const reflection = new Reflection(C1);

console.log('atributos de instância:', reflection.fieldsName);

console.log('métodos de instância:', reflection.methodsName);

console.log('atributos de classe:', reflection.staticFieldsName);

console.log('métodos de classe:', reflection.staticMethodsName);
